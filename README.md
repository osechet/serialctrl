serialctrl
===

serialctrl can be used to modify the data sent on a serial line. It can be useful to simulate data loss or latency.
It works by opening a pseudo-terminal and redirects all the data to/from the given serial port.

## Install

```
go install gitlab.com/osechet/serialctrl
```

## Usage

```
Usage of serialctrl:
  -cut int
    	cut each received buffer and send each part with a delay (default 1)
  -debug
    	print the debug logs
  -latency duration
    	latency to add between each buffer (or each buffer part, when using the cut option)
  -loss float
    	the percentage of byte loss (default 0.01)
  -serial string
    	the serial port to replace (default "/dev/ttyS0")
```

Example:

```
serialctrl -serial /dev/ttyS1 -loss 0.1 -latency 100ms -cut 3
```

Note: the loss is based on the number of received buffers. Each time the program reads the input pseudo-terminal, it creates a buffer. If the loss is 0.1, it means 1 byte will be removed from 10% of the read buffers.

Once the program is started, it will tell you which pseudo-terminal can be used, i.e.:
```
pty created. Attach your program to /dev/pts/2
```

Use this serial port with your application. All the data that is sent or received will go through serialctrl. Only the outgoing data is modified.
