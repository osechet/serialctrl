package main

import (
	"flag"
	"math/rand"
	"os"
	"sync"
	"time"

	"github.com/op/go-logging"
	"github.com/tarm/serial"
	"gitlab.com/osechet/serialctrl/pty"
)

var log = logging.MustGetLogger("serialctrl")

var format = logging.MustStringFormatter(
	`%{color}%{time:15:04:05.000} %{level:.4s} %{id:03x} %{shortfunc} ▶%{color:reset} %{message}`,
)

const defaultBaudrate int = 115200

var debug bool
var serialDevice string
var loss float64
var latency time.Duration
var cut int

func initLogging() {
	console := logging.NewLogBackend(os.Stderr, "", 0)
	consoleFormatter := logging.NewBackendFormatter(console, format)
	consoleLeveled := logging.AddModuleLevel(consoleFormatter)
	if debug {
		consoleLeveled.SetLevel(logging.DEBUG, "")
	} else {
		consoleLeveled.SetLevel(logging.INFO, "")
	}
	logging.SetBackend(consoleLeveled)
}

func removeRandomByte(buffer []byte) []byte {
	idx := int(float64(len(buffer)) * rand.Float64())
	buffer = append(buffer[0:idx], buffer[idx+1:]...)
	log.Debugf("Removed byte at index %d on %d (loss %f%%)", idx, len(buffer))
	return buffer
}

func main() {
	flag.BoolVar(&debug, "debug", false, "print the debug logs")
	flag.StringVar(&serialDevice, "serial", "/dev/ttyS0", "the serial port to replace")
	flag.Float64Var(&loss, "loss", 0.01, "the percentage of byte loss")
	flag.DurationVar(&latency, "latency", 0, "latency to add between each buffer (or each buffer part, when using the cut option)")
	flag.IntVar(&cut, "cut", 1, "cut each received buffer and send each part with a delay")
	flag.Parse()

	initLogging()

	log.Info("Application started")
	log.Info("========================================")

	if cut < 0 {
		cut = 1
	}

	log.Infof("Serial : %s", serialDevice)
	log.Infof("Loss   : %f", loss)
	log.Infof("Latency: %s", latency)
	log.Infof("Cut    : %d", cut)

	ptyPort, err := pty.OpenPty()
	if err != nil {
		log.Fatal(err)
	}
	log.Infof("pty created. Attach your program to %s", ptyPort.Slave)
	defer ptyPort.Close()

	serialPort, err := serial.OpenPort(&serial.Config{
		Name: serialDevice,
		Baud: defaultBaudrate,
	})
	if err != nil {
		log.Fatal(err)
	}
	defer serialPort.Close()

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		defer wg.Done()
		lost := 0
		count := 0
		for {
			buffer := make([]byte, 4096)
			log.Debug("waiting data from pty...")
			n, err := ptyPort.Read(buffer)
			if err != nil {
				log.Fatal(err)
			}
			count++
			buffer = buffer[:n]
			value := rand.Float64()
			if value < loss {
				lost++
				buffer = removeRandomByte(buffer)
				log.Infof("Loss %f%%)", (float64(lost) / float64(count)))
			}

			time.Sleep(latency)

			begin := 0
			end := len(buffer) / cut
			for i := 0; i < cut-1; i++ {
				n, err = serialPort.Write(buffer[begin:end])
				if err != nil {
					log.Fatal(err)
				}
				log.Debugf("%d bytes written to serial (%d/%d)", n, i+1, cut)
				begin = end
				end = end + len(buffer)/cut
				time.Sleep(latency)
			}
			n, err = serialPort.Write(buffer[begin:])
			if err != nil {
				log.Fatal(err)
			}
			log.Debugf("%d bytes written to serial (%d/%d)", n, cut, cut)
		}
	}()
	go func() {
		defer wg.Done()
		for {
			buffer := make([]byte, 4096)
			log.Debug("waiting data from serial...")
			n, err := serialPort.Read(buffer)
			if err != nil {
				log.Fatal(err)
			}
			n, err = ptyPort.Write(buffer[:n])
			if err != nil {
				log.Fatal(err)
			}
			log.Debug("%d bytes written to pty", n)
		}
	}()

	wg.Wait()
	log.Info("Done")
}
