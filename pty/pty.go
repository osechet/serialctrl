// Copyright 2017 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build aix darwin dragonfly freebsd linux,!android netbsd openbsd
// +build cgo

// Package pty is a simple pseudo-terminal package for Unix systems,
// implemented by calling C functions via cgo.
// It is based on the os/signal/internal/pty package of go 1.13.
package pty

/*
#define _XOPEN_SOURCE 600
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
*/
import "C"

import (
	"fmt"
	"os"
	"syscall"
)

// Error is used if error occurs when handling a pty.
type Error struct {
	FuncName    string
	ErrorString string
	Errno       syscall.Errno
}

func ptyError(name string, err error) *Error {
	return &Error{name, err.Error(), err.(syscall.Errno)}
}

func (e *Error) Error() string {
	return fmt.Sprintf("%s: %s", e.FuncName, e.ErrorString)
}

func (e *Error) Unwrap() error { return e.Errno }

// Open returns a master pty and the name of the linked slave tty.
func Open() (master *os.File, slave string, err error) {
	m, err := C.posix_openpt(C.O_RDWR)
	if err != nil {
		return nil, "", ptyError("posix_openpt", err)
	}
	if _, err := C.grantpt(m); err != nil {
		C.close(m)
		return nil, "", ptyError("grantpt", err)
	}
	if _, err := C.unlockpt(m); err != nil {
		C.close(m)
		return nil, "", ptyError("unlockpt", err)
	}
	slave = C.GoString(C.ptsname(m))
	return os.NewFile(uintptr(m), "pty-master"), slave, nil
}

// OpenPty creates a ReadeWriteCloser around the pseudo-terminal.
func OpenPty() (*Pty, error) {
	f, slave, err := Open()
	if err != nil {
		return nil, err
	}
	return &Pty{f, slave}, nil
}

// Pty defines a pseudo-terminal as a ReadWriteCloser.
type Pty struct {
	f *os.File
	// The name of the slave serial port.
	Slave string
}

func (p *Pty) Read(b []byte) (n int, err error) {
	return p.f.Read(b)
}

func (p *Pty) Write(b []byte) (n int, err error) {
	return p.f.Write(b)
}

// Close closes the pseudo-terminal.
func (p *Pty) Close() (err error) {
	return p.f.Close()
}
