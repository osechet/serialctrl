module gitlab.com/osechet/serialctrl

go 1.13

require (
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/sys v0.0.0-20190919044723-0c1ff786ef13
)
